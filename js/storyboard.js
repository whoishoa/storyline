
var StoryBoard = angular.module("StoryBoard", [ "ngRoute" ]);
var controllerScopes = {};

/*
 * This function handles all the page resizing
 */
function refresh() {
	if ($("#splashImg").length > 0) {
		controllerScopes["splashScreenCtrl"].splashImage.currHeight = 
			$(
				"#splashImg").height();
		controllerScopes["splashScreenCtrl"].splashImage.currWidth = $(
				"#splashImg").width();
		controllerScopes["splashScreenCtrl"].$apply();
	}
	$("#footerBar").css("top", $("#StoryBoard").height() - 50 + "px");
}

$(function() {
	
	$(window).resize(function() {
		refresh();
	});

	// JQuery Methods area

});

StoryBoard.run(function($rootScope) {
	refresh();
	$("#menu").hide();
	$rootScope.changeView = function(newView) {
		$("#menu").hide();
		parent.location = "#/" + newView;
	};
	$rootScope.toggleMenu = function() {
		$("#menu").toggle("fast");
	};
	$rootScope.header = {};
	$rootScope.header.setTitle = function(titleImgId, titleLabel) {
		var titleImg = $("#titleDiv").children(1);
	    if (titleImg.hasClass("cardSprite")) {
	    	titleImg.removeClass("cardSprite");
	    	titleImg.addClass("titleSprite");
	    }
	    titleImg.first().attr("id", titleImgId);
	    $("#titleLabel").html(titleLabel);
	}
	$rootScope.header.setCard = function(cardImgId, cardLabel) {
		var titleImg = $("#titleDiv").children(1);
	    if (titleImg.hasClass("titleSprite")) {
	    	titleImg.removeClass("titleSprite");
	    	titleImg.addClass("cardSprite");
	    }
	    titleImg.first().attr("id", cardImgId);
	    $("#titleLabel").html(cardLabel);
	}
});

StoryBoard.config(function($routeProvider) {
	$routeProvider.when("/", {
		templateUrl : "splash.html",
		controller : "splashScreenCtrl"
	}).when("/Main", { // Main Page
		templateUrl : "main.html",
		controller : "mainScreenCtrl"
	}).when("/Select", { // Select A Card page
		templateUrl : "splash.html",
		controller : "splashScreenCtrl"
	}).when("/Edit", { // Edit A Card page
		templateUrl : "splash.html",
		controller : "splashScreenCtrl"
	}).when("/Thanks", { // Thank You page
		templateUrl : "splash.html",
		controller : "splashScreenCtrl"
	}).when("/How", { // How does it work page
		templateUrl : "how.html",
		controller : "howScreenCtrl"
	}).when("/myCarousel", { // How does it work page
		redirectTo : "/How"
	}).when("/Inspiration", { // Thank You page
		templateUrl : "splash.html",
		controller : "splashScreenCtrl"
	}).when("/News", { // News Feed page
		templateUrl : "news.html",
		controller : "newsScreenCtrl"
	}).otherwise({
		redirectTo : "/"
	})
})

StoryBoard.controller("splashScreenCtrl", function($scope) {
	    $("#headerBar").hide();
	    $("#menu").hide();
	
		controllerScopes["splashScreenCtrl"] = $scope;
		
		$scope.splashImage = {
			originalHeight : 604,
			originalWidth : 1213,
			currHeight : $("#splashImg").height(),
			currWidth : $("#splashImg").width()
		};
		/*
		 * The x and y coordinates of where the "You Just Made My Day" Button
		 * should be located relative to the original width and height of 
		 * $scope.splashImage
		 */
		$scope.youJustMadeMyDay = {
			x1 : 241,
			y1 : 243,
			x2 : 453,
			y2 : 455
		}
		$scope.takeATour = {
			x1 : 643,
			y1 : 368,
			x2 : 863,
			y2 : 424
		}
		$scope.shareTheLove = {
			x1 : 880,
			y1 : 368,
			x2 : 1142,
			y2 : 424
		}

		this.scaleX = function(currX) {
			return currX / $scope.splashImage.originalWidth
					* $scope.splashImage.currWidth;
		}

		this.scaleY = function(currY) {
			return currY / $scope.splashImage.originalHeight
					* $scope.splashImage.currHeight;
		}

		return $scope.splashScreenCtrl = this;
	});



StoryBoard.controller("howScreenCtrl", function($scope) {
	$("#headerBar").show();
	$scope.header.setTitle("titleHowSprite", "How It Works");
	
	controllerScopes["howScreenCtrl"] = $scope;
	$('#myCarousel').carousel({  
		  interval: 5000
	});
});

StoryBoard.controller("newsScreenCtrl", function($scope) {
	$("#headerBar").show();
	$scope.header.setTitle("titleNewsSprite", "News Feed");
	$scope.currUser = "Mark Kocher";
	$scope.newsFeed = [
	                   {picture:"imgs/samplePic.png",card:"imgs/sampleCard.png", from:"Mark Kocher",time:"3 mins ago"},
	                   {picture:"imgs/samplePic.png",card:"imgs/sampleCard.png", from:"Mark Kocher",time:"3 mins ago"},
	                   {picture:"imgs/samplePic.png",card:"imgs/sampleCard.png", from:"Mark Kocher",time:"3 mins ago"},
	                   {picture:"imgs/samplePic.png",card:"imgs/sampleCard.png", from:"Mark Kocher",time:"3 mins ago"},
	                   {picture:"imgs/samplePic.png",card:"imgs/sampleCard.png", from:"Mark Kocher",time:"3 mins ago"},
	                   {picture:"imgs/samplePic.png",card:"imgs/sampleCard.png", from:"Mark Kocher",time:"3 mins ago"},
	                   {picture:"imgs/samplePic.png",card:"imgs/sampleCard.png", from:"Mark Kocher",time:"3 mins ago"},
	                   {picture:"imgs/samplePic.png",card:"imgs/sampleCard.png", from:"Mark Kocher",time:"3 mins ago"},
	                   {picture:"imgs/samplePic.png",card:"imgs/sampleCard.png", from:"Mark Kocher",time:"3 mins ago"},
	                   {picture:"imgs/samplePic.png",card:"imgs/sampleCard.png", from:"Mark Kocher",time:"3 mins ago"},
	                   {picture:"imgs/samplePic.png",card:"imgs/sampleCard.png", from:"Mark Kocher",time:"3 mins ago"}];
	
	
});